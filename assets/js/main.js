// --------------------------------------------------------------------------
;( function( $, window, document, undefined ){ 'use strict';
    $( document ).ready( function(){
        // ------------------------------------------------------------------
        var $header = $('header.header');
        var $navPrimary = $('.nav-primary');
        var $sidebar = $('.sidebar.nav-main');
        // ------------------------------------------------------------------
        var $btnSlideout = $sidebar.find('.btn-slideout');
        var $btnSlidein = $sidebar.parent().find('.btn-slidein');
        var sidebarOnscreen = 'sidebar-onscreen';
        // ------------------------------------------------------------------

        // ------------------------------------------------------------------
        // Toggle the main-nav/sidebar on mobile screen
        // ------------------------------------------------------------------
        $btnSlidein.on( 'click', function(){
            $navPrimary.toggleClass( sidebarOnscreen );
        });
        // ------------------------------------------------------------------
        // Close the sidebar
        // ------------------------------------------------------------------
        $btnSlideout.on( 'click', function(){
            $navPrimary.removeClass( sidebarOnscreen );
        });
        // ------------------------------------------------------------------


        // ------------------------------------------------------------------
        var $navSecondary = $('.nav-secondary');
        var $navSupport = $('.nav-support');
        var $btnSupport = $navPrimary.find('.btn-support');
        var supportOnscreen = 'support-onscreen';
        // ------------------------------------------------------------------
        // Toggle open the secondary/support navigation
        // ------------------------------------------------------------------
        $btnSupport.on( 'click', function(){
            if( !$navSecondary.hasClass( supportOnscreen )){
                $navSecondary.addClass( supportOnscreen );
            }
        });
        // ------------------------------------------------------------------
        // Close the support navigation when user clicks outside the nav
        // ------------------------------------------------------------------
        $( document ).mouseup( function(e){
            // --------------------------------------------------------------
            // Close support navigation
            // --------------------------------------------------------------
            if( $navSecondary.hasClass( supportOnscreen )){
                var notTarget = !$navSupport.is( e.target ) && $navSupport.has( e.target ).length === 0;
                var notButton = !$btnSupport.is( e.target ) && $btnSupport.has( e.target ).length === 0;
                if( notTarget && notButton ) $navSecondary.removeClass( supportOnscreen );
            }
            // --------------------------------------------------------------
            if( $navPrimary.hasClass( sidebarOnscreen )){
                var notTarget = !$sidebar.is( e.target ) && $sidebar.has( e.target ).length === 0;
                if( notTarget ) $navPrimary.removeClass( sidebarOnscreen );
            }
            // --------------------------------------------------------------
        });
        // ------------------------------------------------------------------


        // ------------------------------------------------------------------
        // Add sticky behaviour to the header
        // ------------------------------------------------------------------
        $header.stickybits({
            stickyClass: 'sticks',
            useStickyClasses: true
        });
        // ------------------------------------------------------------------
    });
}( jQuery, window, document ));
// --------------------------------------------------------------------------